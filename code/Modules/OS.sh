# os version and information
os_name=$(uname -s)
os_version=$(cat /etc/os-release | grep ^PRETTY_NAME= | cut -c13-)

# date
now=$(date)

# x64 or x86
os_type=$(uname -m)

echo "os name: $os_name
os version: $os_version
current date: $now
os type: $os_type"
