# password
pass=$(passwd --status $(echo "$USER"))
# admin
if [[ "$(id)" =~ "27(sudo)" ]]; then
    admin="Yes"
else
    admin="No"
fi

echo "User Password: $pass
User Are Admin? $admin"
