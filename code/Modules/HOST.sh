# host info
hostname=$(hostname)

# user name
user_name=$USERNAME

# user directory
user_dir=$(echo $HOME)

# hard disk
partitions=$(grep -c 'sda[0-9]' /proc/partitions)

# apps list
apps=$(for app in /usr/share/applications/*.desktop; do echo "${app:24:-8}"; done)

echo "Username: $user_name
Host Name: $hostname
partitions number: $partitions
Applications List:
$apps"
