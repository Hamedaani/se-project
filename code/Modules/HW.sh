# mother-board
moboard_name="$(cat /sys/class/dmi/id/board_vendor) - $(cat /sys/class/dmi/id/board_name)"
bios_version=$(cat /sys/class/dmi/id/bios_version)

# hard-disk
hard_manufactor=$(cat  /sys/block/sda/device/vendor)
hard_model=$(cat  /sys/block/sda/device/model)
hard_size=$(cat  /sys/block/sda/size)
hard_serial=$(cat /sys/block/sda/device/wwid)

# cd-rom
cdrom_vendor=$(cat /sys/block/sr0/device/vendor)
cdrom_name=$(cat /sys/block/sr0/device/model)

# cpu
cpu_name=$(lscpu | sed -nr '/Model name/ s/.*:\s*(.*) @ .*/\1/p')
cpu_cores=$(lscpu | grep -E '^CPU\(')

#ram
#ram_size=$(cat proc/meminfo | grep MemTotal)

# gpu
#gpu_info=$(lshw -C display)

echo "motherboard name: $moboard_name
bios version: $bios_version

hard-disk manufactor: $hard_manufactor
hard-disk model: $hard_model
hard-disk serial: $hard_serial
hard-disk size: $hard_size

cd-rom manufactor: $cdrom_vendor
cd-rom name: $cdrom_name

cpu name: $cpu_name
cpu cores number: $cpu_cores"
